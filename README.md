# Quotas
![Quotas](./screenshot.jpg) 

## Description
Quotas is a module for Gallery 3 that will track how much total space is being used to store each users photos and movies and will allow admins to place upload limits on user groups.

Once activated, this module will keep a running total (in bytes) of the file sizes for photos and movies as each user uploads and deletes files (albums are ignored). If a file would push a user over their limit, it will be automatically rejected on upload, with the user seeing a message that Gallery was unable to process their file.

Be aware that any module that can reassign a users photos to another user (other then deleting a user) or modify a file (other then rotating) will mess up this total file size number as the change in file size will not be logged.

This module contains a maintenance script to calculate the current disc space usage for all existing files and users. It can be found at Admin -> Maintenance -> Rebuild user quotas table.

This module will also display a message at the bottom of album/photo/movie pages for logged in users to tell them how much space they're using, and what their limit is (if they have one).

Admin users may log in and see a current user's usage from the Admin -> Content -> User quotas screen.
Admin users may assign upload limits (in megabytes) from the Admin -> Content -> User quotas screen. Admin's may also specify if resizes and thumbnails should be counted towards a users limit from this page.

Once a limit is set, all users in that group will be unable to upload more then their limit, with the exception that if a full sized image does not put the user over their limit, but the resize+thumb does, the photo will still be accepted (as resizes and thumbs are not generated until after the photo is stored in Gallery and after the quota check is run). A value of 0 can be used to specify "no limit" for a group. If a user is assigned to more then one group, the largest non-zero value will be used as their limit.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "quotas" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 1.1.0:**
> - Added a "User stats" section to the user profile pages -- displays number of albums, number of uploads (photos and movies) and disc usage.
> - Released 13 June 2012.
>
> Download: [Version 1.1.0](/uploads/e8ae5a1395a16827a376384fdde5d916/quotas110.zip)

**Version 1.0.1:**
> - Updated to use an appropriate file size prefix instead of displaying everything in megabytes.
> - Now displays an infinity symbol instead of "0.00 MB" for users without an upload limit.
> - Tested with Gallery 3.0.3.
> - Released 16 April 2012.
>
> Download: [Version 1.0.1](/uploads/997330e21eae15f2cc8d54779f78af70/quotas101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 12 September 2011
>
> Download: [Version 1.0.0](/uploads/d223bc610501e8e7ecf54ae691e0fe2a/quotas100.zip)
